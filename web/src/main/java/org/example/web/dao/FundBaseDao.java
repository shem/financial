package org.example.web.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;
import org.example.web.entity.FundBase;

@Mapper
public interface FundBaseDao {

    @Insert("insert info fund_base() values(#{}, #{}, #{}, #{}, #{}, " +
            "#{}, #{}, #{}, #{}, #{})")
    public int insert(FundBase base);

    @Delete("delete from fund_base where id = #{param1}")
    public int delete(Integer id);

    @Update("")
    public int update(FundBase base);
}
