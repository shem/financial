package org.example.web.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FundBase {

    private Integer id;

    /**
     * 基金编码
     */
    private String code;

    /**
     * 基金简称
     */
    private String name;

    /**
     * 基金全称
     */
    private String fullname;

    /**
     * 基金经理
     */
    private String manager;

    /**
     * 所属机构
     */
    private String org;

    /**
     * 资产规模
     */
    private BigDecimal asset;

    /**
     * 基金类型
     */
    private Integer type;

    /**
     * 发行日期
     */
    private Date issuingDate;

    /**
     * 发售平台
     */
    private String salePlatform;

    /**
     * 录入时间
     */
    private Date recordTime;

}
