package org.example.web.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserFundAssetChangeInfo {

    private Integer id;

    /**
     * 基金编码
     */
    private String code;

    /**
     * 持有金额
     */
    private BigDecimal holdAsset;

    /**
     * 变动时间
     */
    private BigDecimal changeTime;
}
