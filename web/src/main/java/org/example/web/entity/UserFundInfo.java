package org.example.web.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserFundInfo {

    private Integer id;

    /**
     * 基金编码
     */
    private String code;

    /**
     * 投入成本
     */
    private BigDecimal inputCost;

    /**
     * 持有资产
     */
    private BigDecimal holdAsset;

    /**
     * 更新时间
     */
    private Date latestTime;


}
