package org.example.web.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserFundTradeInfo {

    private Integer id;

    /**
     * 基金编码
     */
    private String code;

    /**
     * 交易类型【1:转入，2:转成】
     */
    private Integer type;

    /**
     * 交易金额
     */
    private BigDecimal tradeAmount;

    /**
     * 交易时间
     */
    private Date tradeTime;

    /**
     * 手续费
     */
    private BigDecimal charge;

    /**
     * 备注
     */
    private String remarks;
}
